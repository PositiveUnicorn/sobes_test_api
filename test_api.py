import requests
import json
from flask import Flask, json
import sqlite3 as sql


def output_api(code_word):
  response = requests.get("https://ghibliapi.herokuapp.com/films")
  todos = json.loads(response.text)
  zapor = []
  for todo in todos:
      zapor.append(todo[code_word])
  return(zapor)

def get_db():
  con = sql.connect('test.db')
  data_basa=[]
  with con:
      cur = con.cursor()
      cur.execute("SELECT test.name FROM `test`")
      rows = cur.fetchall()
      for row in rows:
          data_basa.append(row[0])
      cur.close()
  return(data_basa)


def get_zapros_in_db():
  choice = int(input("> "))
  con = sql.connect('test.db')
  with con:
      cur = con.cursor()
      cur.execute("CREATE TABLE IF NOT EXISTS `test` (`name` STRING)")
      name = input("Name\n> ")
      cur.execute(f"INSERT INTO `test` VALUES ('{name}')")
      con.commit()
      cur.close()

def result_generator():
  result = [{"id": 'ghibli', "name": output_api('title')}, {"id": 'marvel', "name": get_db()}]
  return(result)

api = Flask(__name__)

@api.route('/films', methods=['GET'])
def get_result():
  return json.dumps(result_generator())

if __name__ == '__main__':
    api.run()

